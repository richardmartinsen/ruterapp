﻿using Newtonsoft.Json;
using NServiceBus;
using RuterApp.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using JsonSerializer = NServiceBus.JsonSerializer;

namespace RuterApp.Klient
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncMain().GetAwaiter().GetResult();
        }

        static async Task AsyncMain()
        {
            Console.Title = "Client";
            Console.WriteLine("Client...");

            var endpointConfiguration = new EndpointConfiguration(
                endpointName: "RuterApp.Client");

            endpointConfiguration.SendFailedMessagesTo("error");
            endpointConfiguration.UseSerialization<JsonSerializer>();
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();

            var endpointInstance = await Endpoint.Start(endpointConfiguration)
                .ConfigureAwait(false);
            try
            {
                await SendHoldeplassInfo(endpointInstance);
            }
            finally
            {
                await endpointInstance.Stop()
                    .ConfigureAwait(false);
            }
        }

        static async Task SendHoldeplassInfo(IEndpointInstance endpointInstance)
        {
            Console.WriteLine("Press enter to send a message");
            Console.WriteLine("press any key to exit");

            while (true)
            {
                var key = Console.ReadKey();
                Console.WriteLine();

                if (key.Key != ConsoleKey.Enter)
                {
                    return;
                }

                //var urlGetLines = "http://reisapi.ruter.no/Line/GetLines?json=true";
                //var rLines = _download_serialized_json_data<List<RuterLine>>(urlGetLines);


                //foreach (var r in rLines)
                //{
                //    // Transportation 8 == Metro
                //    if (r.Transportation == 8)
                //    {

                //        var l = new Line();
                //        l.LineId = r.ID;
                //        l.Name = r.Name;
                //        l.Transportation = r.Transportation;
                //        l.LineColor = r.LineColour;

                //        Console.WriteLine("Linje : " + l.LineId + ' ' + l.Name);
                //        //_context.Lines.Add(l);
                //        //_context.SaveChanges();

                //        // Hente ut alle stoppestedene for linjen vi akkurat har funnet
                //        var urlGetStopsByLine = "http://reisapi.ruter.no/Line/GetStopsByLineId/" + r.ID + "?json=true";
                //        var rStopsByLine = _download_serialized_json_data<List<RuterStopsByLine>>(urlGetStopsByLine);

                //        int order = 0;
                //        foreach (var sl in rStopsByLine)
                //        {

                //            var s = new StopByLine();
                //            order++;
                //            s.LineId = r.ID;
                //            s.StopId = sl.ID;
                //            s.Order = order;

                //            Console.WriteLine("Stop By Line : " + s.LineId + ' ' + s.StopId + ' ' + s.Order);

                //            //_context.StopsByLine.Add(s);
                //            //_context.SaveChanges();

                //            // Finne data for et stoppested
                //            var urlGetStop = "http://reisapi.ruter.no/place/getstop/" + sl.ID + "?json=true";
                //            var rStop = _download_serialized_json_data<RuterStop>(urlGetStop);

                //            var stop = new Stop();
                //            stop.StopId = rStop.ID;
                //            stop.Name = rStop.Name;
                //            stop.ShortName = rStop.ShortName;
                //            Console.WriteLine("Stop : " + stop.StopId + ' ' + stop.Name + ' ' + stop.ShortName);
                //            // Sjekke om raden finnes allerede i basen
                //            //var finnes = _context.Stops.FirstOrDefault(x => x.StopId == stop.StopId);
                //            //if (finnes == null)
                //            //{
                //            //    _context.Stops.Add(stop);
                //            //    _context.SaveChanges();
                //            //}
                //        }
                //    }
                //}
                

                var urlGetStopVisit = "http://reisapi.ruter.no/StopVisit/GetDepartures/3010011?json=true"; //jernbanetorget
                
                var Departures = _download_serialized_json_data<List<Departure>>(urlGetStopVisit);

                foreach (var departure in Departures)
                {
                    var east = false;
                    var west = false;
                    var ret = "";

                    if (departure.MonitoredVehicleJourney.DirectionRef == "1")
                    {
                        east = true;
                        ret = "Øst";
                    }
                    else
                    {
                        west = true;
                        ret = "Vest";
                    }
                    
                    
                    var d = new HoldeplassInfo
                    {
                        HoldeplassNavn = "Jernbanetorget",
                        Linjenavn = departure.MonitoredVehicleJourney.DestinationName,
                        HoldeplassId = departure.MonitoringRef,
                        Linjenr = departure.MonitoredVehicleJourney.LineRef,
                        TidTilAnkomst = departure.MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime - DateTime.Now,
                        Forsinkelse = departure.MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime - departure.MonitoredVehicleJourney.MonitoredCall.AimedArrivalTime,
                        Holdeplass = departure.MonitoredVehicleJourney.MonitoredCall.VehicleAtStop,
                        Id = departure.MonitoredVehicleJourney.VehicleRef,
                        East = east,
                        West = west,
                        Retning = ret
                        
                    };
                    await endpointInstance.Send("RuterApp.Server", d);
                }

                urlGetStopVisit = "http://reisapi.ruter.no/StopVisit/GetDepartures/3010020?json=true"; //Stortinget
                Departures = _download_serialized_json_data<List<Departure>>(urlGetStopVisit);
                foreach (var departure in Departures)
                {
                    var d = new HoldeplassInfo
                    {
                        HoldeplassNavn = "Stortinget",
                        Linjenavn = departure.MonitoredVehicleJourney.DestinationName,
                        HoldeplassId = departure.MonitoringRef,
                        Linjenr = departure.MonitoredVehicleJourney.LineRef,
                        TidTilAnkomst = departure.MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime - DateTime.Now,
                        Forsinkelse = departure.MonitoredVehicleJourney.MonitoredCall.ExpectedArrivalTime - departure.MonitoredVehicleJourney.MonitoredCall.AimedArrivalTime,
                        Holdeplass = departure.MonitoredVehicleJourney.MonitoredCall.VehicleAtStop,
                        Id = departure.MonitoredVehicleJourney.VehicleRef

                    };
                    await endpointInstance.Send("RuterApp.Server", d);
                }

                //var h = new HoldeplassInfo
                //{
                //    HoldeplassNavn = "Jernbanetorget",
                //    HoldeplassId = 111,
                //    AvstandFraForrige = 43,
                //    East = true,
                //    West = false,
                //    Forsinkelse = TimeSpan.FromSeconds(20),
                //    Linjenr = 5,
                //    TidTilAnkomst = TimeSpan.FromSeconds(50)
                //};

                //await endpointInstance.Send("RuterApp.Server", h);
                //Console.WriteLine($"Sent info om holdeplass: {h.HoldeplassNavn:N}");
            }
        }

        private static T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                // attempt to download JSON data as a string
                try
                {
                    w.Encoding = Encoding.UTF8;
                    json_data = w.DownloadString(url);
                }
                catch (Exception) { }
                // if string with JSON data is not empty, deserialize it to class and return its instance 
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }
    }
}
