﻿namespace RuterApp.Klient
{
    public class StopByLine
    {
        public int StopByLineId { get; set; }
        public int LineId { get; set; }
        public int StopId { get; set; }
        public int Order { get; set; }
    }
}
