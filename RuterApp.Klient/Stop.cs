﻿namespace RuterApp.Klient
{
    public class Stop
    {
        public int Id { get; set; }
        public int StopId { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
    }
}
