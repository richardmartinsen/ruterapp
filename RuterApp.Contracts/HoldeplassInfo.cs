﻿using NServiceBus;
using System;

namespace RuterApp.Contracts
{
    public class HoldeplassInfo : IMessage
    {
        public string HoldeplassId { get; set; }
        public string HoldeplassNavn { get; set; }
        public string Linjenavn { get; set; }
        public string Linjenr { get; set; }
        public Boolean East { get; set; }
        public Boolean West { get; set; }
        public TimeSpan TidTilAnkomst { get; set; }
        public int AvstandFraForrige { get; set; }
        public TimeSpan Forsinkelse { get; set; }
        public Boolean Holdeplass { get; set; }
        public string Id { get; set; }
        public string Retning { get; set; }

    }
}
